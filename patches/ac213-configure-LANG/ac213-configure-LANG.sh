# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# Patch all 2.13 configures to be safe
patch_targets() {
    fgrep -r --include configure -l "Generated automatically using autoconf version 2.13" "${WORKDIR}"
}

# This patch is always required
patch_required() {
    return 0
}
