# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

patch_targets() {
    find "${WORKDIR}" -name config.guess -or -name config.sub
}

patch_required() {
    return 0
}

patch_trigger_action() {
    [[ -f "$1" ]] || return -1

    local base_param=$(basename "$1")

    cp "${PREFIX}/share/gnuconfig/${base_param}" "$1"
}
