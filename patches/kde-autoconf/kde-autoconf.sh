# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

patch_targets() {
    # If we are not using autoconf 2.6, just skip the patch for the sake of
    # limiting the resulting patche
    [[ $(WANT_AUTOCONF=2.5 autoconf --version | head -n 1) == autoconf*2.6* ]] \
	|| return 0

    # Find the admindir, wherever it is.
    find "${WORKDIR}" -path '*/admin/cvs.sh' -type f -exec egrep -L 'autoconf.*2.6.*' {} +
}

# This patch is always required
patch_required() {
    return 0
}
