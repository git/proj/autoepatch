# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

patch_targets() {
    # Find the configure scripts with a libtool that checks for freebsd
    # versioning format, and that were not patched with our patch already
    fgrep --null -l --include configure "version_type=freebsd-" "${WORKDIR}" | \
	xargs -0 fgrep -L 'Handle Gentoo/FreeBSD as it was Linux' | \
	fgrep -v '(standard input)'
}

# This patch is required only on Gentoo/FreeBSD
patch_required() {
    [[ ${CHOST} == *-gentoo-freebsd* ]] || return 1

    return 0
}
