# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

patch_targets() {
    fgrep -rl --include 'detect-autoconf.sh' 'checkAutoconf' "${WORKDIR}"
}

# Never required, but always suggested
patch_required() {
    return 1
}

patch_trigger_action() {
    [[ -f "$1" ]] || return 1

    ebegin "Replacing detect-autoconf.sh with a dumber one"

    cat - > "$1" <<EOF
#!/bin/sh
export AUTOCONF="autoconf"
export AUTOHEADER="autoheader"
export AUTOM4TE="autom4te"
export AUTOMAKE="automake"
export ACLOCAL="aclocal"
export WHICH="which"
EOF
    eend $?

    return 0
}
