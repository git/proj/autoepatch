#!/bin/bash
# autoepatch - Automatic patch scripting
# Copyright (C) 2006 Gentoo Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with autoepatch; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

main() {
    # To allow installing on any prefix, make sure that
    # the prefix is actually set
    local PREFIX="@PREFIX@"
    [[ ${PREFIX//@/|} == "|PREFIX|" ]] && PREFIX="/usr"

    # When running out of SVN, load local modules and
    # libraries only.
    local basedir="$(dirname $0)"
    [[ $0 == "${PREFIX}/bin/autoepatch.sh" ]] && \
	basedir="${PREFIX}/share/autoepatch"

    # Source baselayout functions.sh for einfo/ewarn and similar
    # functions. If on an offset prefix, use the prefixed path.
    [[ ${PREFIX} == "/usr" ]] && \
	source "/sbin/functions.sh" || \
	source "${PREFIX}/sbin/functions.sh"

    source "${basedir}/lib/functions.sh"

    [[ -z ${CHOST} ]] && CHOST="$(portageq envvar CHOST)"
    [[ -z ${WORKDIR} ]] && WORKDIR="$(pwd)"
    [[ -z ${T} ]] && T="/tmp"

    for patchset in "${basedir}"/patches/*; do
	(
	    source "${patchset}/${patchset##*/}.sh"
	    targets="$(patch_targets)"
	    [[ -z ${targets} ]] && exit 0

	    einfo "Applying ${patchset##*/} ..."

	    while read target; do
		ebegin " on ${target##$(pwd)/} ..."

		for patch in "${patchset}"/*.patch; do
		    if try_patch "${target}" "${patch}"; then
			PATCH_APPLIED="yes"
			break
		    fi
		done
		
		if type patch_trigger_action &>/dev/null; then
		    if patch_trigger_action "${target}"; then
			PATCH_APPLIED="yes"
		    fi
		fi

		# Check if the patchset requires us to fail if the
		# patch is not applied. By default, don't.
		if ! type patch_required &>/dev/null; then
		    patch_required() { return 1; }
		fi
		
		if ! type patch_failed_msg &>/dev/null; then
		    patch_failed_msg() { 
			eerror "Failed patch ${patchset##*/}"
		    }
		fi

		if [[ -n ${PATCH_APPLIED} ]]; then
		    eend 0
		else
		    eend 1
		    if patch_required; then 
			patch_failed_msg && exit 2
		    else
			ewarn "  failed, but patch not required, ignoring."
		    fi
		fi
	    done <<<"${targets}"

	    rm -f "${T}"/autoepatch.$$.*
	    exit 0
	)

	case $? in
	    0) ;; # All gone well
	    1) eerror "Error in subshell"; return 1 ;; # Something didn't go well
	    2) return 1;; # The patch failed to apply, already announced.
	esac
    done
}

main
